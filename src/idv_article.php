<?php include "connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/styles.css" />
</head>

<body>
<div class="idv-article-body">
<?php include "header.php"?>

<div class="go_back">
<a href="index.php?id=<?php echo $id; ?>">
<img src="./../images/go-back.png" alt="back arrow">
Return
</a>
</div>

<br></br>

<?php 
$id = $_GET["id"];
$author_list = "SELECT articles.*, authors.firstname , authors.lastname, authors.id AS id2 FROM articles INNER JOIN authors ON articles.author_id = authors.id  WHERE articles.id  = $id ORDER BY publised_at DESC";
$response_author  = $conn->query($author_list);
    foreach($response_author as $idv_article){ 
?>
    <h2 style= "border-bottom: solid 3px #F5832C;">
    <?php echo $idv_article["title"];
    ?> 
    </h2>
    <div class="article-image">
    <img src="<?php echo $idv_article["image_url"];
    ?>" alt="<?php echo $idv_article["image_alt"];
    ?>">
    </div>
    <p style= "color: #F5832C; font-weight: bold;">
    <?php echo $idv_article["publised_at"];
    ?>
    </p>
    <div class="article_destp">
        <p>
        <?php echo $idv_article["content"];
        ?>
        </p>
    </div>
<?php
} ?>
<div class="author-and-tags">
    <p>
    By
    <?php 
    echo $idv_article["firstname"] . " " . $idv_article["lastname"];
    ?>
    </p>
<div class="tags">
    <?php
        $cat_list = "SELECT * FROM articles_categories JOIN categories ON categories.id = articles_categories.category_id WHERE articles_categories.article_id  = $id ";
        $response_cat  = $conn->query($cat_list);
        while($tags = $response_cat->fetch_assoc()){ 
    ?>
        <button>
            <?php echo $tags["category"]; ?>
        </button>
    <?php
    }
    ?>
</div>  
</div>
</div>
<?php include "footer.php"?>
</body>
</html>
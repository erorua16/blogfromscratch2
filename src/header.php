<?php include "connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/styles.css" />
</head>
<body>
    <header>
       <div class="line-row">
                <img src="./../images/icon-for-blog-28.jpg" alt="logo">
                <h1>Blog From Scratch</h1>
       </div>
    </header>
    <div class="filters">
        <div class="filter_authors">
            <?php 
                $author_list = "SELECT authors.firstname , authors.lastname, authors.id FROM authors;";
                $response_author  = $conn->query($author_list);
            ?>
            <select>
            <option hidden>Filter by author</option>
                <?php
                foreach($response_author as $author_list){ 
                ?>
            <option>
            <?php 
                echo $author_list["firstname"] . " " . $author_list["lastname"];
            ?>
            </option>
            <?php
            } ?>
            </select>
        </div>

        <div class="filter_categories">
            <?php 
            $categories_list = "SELECT * FROM categories;";
            $response_category  = $conn->query($categories_list);
            ?>
            <select>
            <option hidden>Filter by categories</option>
                <?php
                foreach($response_category as $categories_list){ 
                ?>
            <option>
                <?php echo $categories_list["category"];
                ?> 
            </option>
            <?php
            } ?>
            </select>
        </div>
    </div>
</body>
</html>
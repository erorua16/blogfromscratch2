<?php include "connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./css/styles.css" />
</head>
<body>
<div class="idv-article-body ">
<?php include "header.php"?>
<div class="articles">  
    <?php
        $articles_list = "SELECT articles.*, authors.firstname , authors.lastname, authors.id AS id2 FROM articles INNER JOIN authors ON articles.author_id = authors.id ORDER BY publised_at DESC";
        $response_articles  = $conn->query($articles_list);
    ?>
    <?php
        foreach($response_articles as $articles_list) {
            $id = $articles_list["id"];
    ?>
    <div class="all-of-article">
        <div class="article-stuff">
    <h2>
    <a href="idv_article.php?id=<?php echo $id; ?>">
    <?php
    echo $articles_list["title"];
    ?>
    </a>
    </h2>
    <div class="line-row">
        <p style= "color: #F5832C; font-weight: bold;">
        by
            <?php 
            echo $articles_list["firstname"] . " " . $articles_list["lastname"];
            ?>
        </p>
        <p style= "color: #F5832C; font-weight: bold;">
            <?php
            echo $articles_list["publised_at"];
            ?>
        </p>
    </div>
    <div class="article_destp">
    <p>
        <?php
        echo $articles_list["content"];
        ?>
    </p>
    </div>
    <div class="read-more">
    <a href="idv_article.php?id=<?php echo $id; ?>">read more</a>
    </div>
   <?php
        $cat_list = "SELECT * FROM articles_categories JOIN categories ON categories.id = articles_categories.category_id WHERE articles_categories.article_id  = $id ";
        $response_cat  = $conn->query($cat_list);
        while($tags = $response_cat->fetch_assoc()){ 
    ?>

       <button  style="float: right;">
            <?php echo $tags["category"]; ?>
        </button>

    <?php
        }
    ?>
    </div>
    </div>
<?php
} ?>
</div>
</div>
<?php include "footer.php"?>
</body>
</html>